#pragma once

#include "game_rules.hpp"
#include "game_state.hpp"
#include "net.hpp"

namespace tetrisai {

class game_client
{
public:
	game_client(const cmd_line &cmd, const std::string_view name);
	~game_client() = default;

	game_client(const game_client &) = delete;
	game_client(const game_client &&) = delete;

	game_client& operator=(const game_client &) = delete;
	game_client& operator=(const game_client &&) = delete;

	const game_state &state() const { return m_state; }
	const game_rules &rules() const { return m_rules; }

	bool wait_for_tick();
	void send_actions(std::string_view actions);

protected:
	game_state m_state;
	net::tcp_client m_client;
	std::string m_name;
	game_rules m_rules;
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//---------------------------------------------------------------------------------------------------------------------
game_client::game_client(const cmd_line &cmd, const std::string_view name)
	: m_rules(cmd)
	, m_name(name)
{
	std::string addr = "127.0.0.1";
	int port = net::default_server_port;

	if (cmd.has_option("server"))
	{
		auto serverAddr = cmd.option_string("server");
		auto portDiv = serverAddr.find(':');

		if (portDiv != std::string::npos)
		{
			port = std::stoi(serverAddr.substr(portDiv + 1));
			addr = serverAddr.substr(0, portDiv);
		}
		else
			addr = serverAddr;
	}

	if (!m_client.connect(addr, port))
		throw std::runtime_error("Could not connect to server!");

	m_client.set_timeout(m_rules.max_tick_time);

	m_client.write(m_name);
}

//---------------------------------------------------------------------------------------------------------------------
bool game_client::wait_for_tick()
{
	auto msg = m_client.read();
	if (msg == "QUIT")
		return false;

	m_state.deserialize(msg);
	return !m_state.playfield.game_over();
}

//---------------------------------------------------------------------------------------------------------------------
void game_client::send_actions(std::string_view actions)
{
	m_client.write(actions);
}

} // namespace tetrisai
