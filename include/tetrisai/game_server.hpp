#pragma once

#include "game_client.hpp"

namespace tetrisai {

class game_server
{
public:
	game_server(const cmd_line &cmd, int playerCount);
	~game_server();

	game_server(const game_server &) = delete;
	game_server(const game_server &&) = delete;

	game_server& operator=(const game_server &) = delete;
	game_server& operator=(const game_server &&) = delete;

	const game_rules &rules() const { return m_rules; }

	void wait_for_end_of_game();

	int tick_count() const { return m_tickCount; }

	void visualize(int mode) const;

	std::string overview_json();

protected:
	void run_local();

	bool wait_for_players(int count);

	bool tick();

	int m_waitForPlayers = 1;

	game_rules m_rules;

	std::unique_ptr<std::thread> m_thread;

	std::unique_ptr<net::tcp_server> m_server;

	std::chrono::steady_clock::time_point m_gameStart;

	std::chrono::steady_clock::time_point m_gameEnd;

	struct player
	{
		int id = 0;
		std::string name;

		std::shared_ptr<net::tcp_client> client;
		game_state state;
		size_t next_piece_index = 0;
		size_t next_garbage_index = 0;
	};

	struct player_overview
	{
		int player_id;
		std::string player_name;

		int cleared_lines;
		int score;
	};

	std::map<int, player_overview> m_playerOverview;

	void handle_player_actions(player &p, std::string_view actions);

	std::vector<player> m_players;

	std::vector<std::shared_ptr<net::tcp_client>> m_connections;

	std::vector<piece> m_randomPieces;

	std::vector<uint16_t> m_randomGarbage;

	using winners = std::vector<int>;

	winners m_winners;

	int m_tickCount = 0;

	int m_visualizeMode;

	int m_visualizeInterval;
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//---------------------------------------------------------------------------------------------------------------------
game_server::game_server(const cmd_line &cmd, int playerCount)
	: m_rules(cmd)
	, m_waitForPlayers(playerCount)
{
	m_visualizeMode = cmd.option_int("visualize_mode", 1);
	m_visualizeInterval = cmd.option_int("visualize_interval", 1);

	auto pieceRange = m_rules.extended_piece_set
		? piece::extended_piece_count
		: piece::standard_piece_count;

	for (int i = 0; i < 7919; ++i)
		m_randomPieces.push_back(piece::create(rand() % pieceRange));

	for (int i = 0; i < 511; ++i)
	{
		uint16_t g = 0;
		while (!g || g == 1023)
			g = rand() % 1024;

		m_randomGarbage.push_back(g);
	}

	// Run locally?
	if (!cmd.has_option("server"))
	{
		m_thread = std::make_unique<std::thread>(&game_server::run_local, this);
		std::this_thread::sleep_for(std::chrono::seconds(1));
	}
}

//---------------------------------------------------------------------------------------------------------------------
game_server::~game_server()
{
	m_connections.clear();

	if (m_thread)
		m_thread->join();
}

//---------------------------------------------------------------------------------------------------------------------
void game_server::run_local()
{
	m_server = std::make_unique<net::tcp_server>(net::default_server_port);
	wait_for_players(m_waitForPlayers);

	m_gameStart = std::chrono::steady_clock::now();

	try
	{
		while (tick())
		{
			auto current = std::chrono::steady_clock::now();
			auto diff = current - m_gameStart;

			if (diff >= std::chrono::milliseconds(m_rules.max_game_time))
			{
				break;
			}

			//std::this_thread::sleep_for(std::chrono::milliseconds(500));
		}
	}
	catch (const std::exception& e)
	{
		std::cerr << "Exception [game_server]: " << e.what() << std::endl;
	}

	m_gameEnd = std::chrono::steady_clock::now();
}

//---------------------------------------------------------------------------------------------------------------------
bool game_server::wait_for_players(int count)
{
	if (count == 1)
	{
		m_rules.last_man_standing = false;
		m_rules.send_garbage = false;
	}

	for (int i = 0; i < count; ++i)
	{
		player p;
		p.id = i + 1;
		p.client = m_server->accept_client();
		p.client->set_timeout(m_rules.max_tick_time);

		if (!p.client)
			return false;

		p.name = p.client->read();

		p.state.current = m_randomPieces[p.next_piece_index++];
		if (m_rules.known_next_pieces > 0)
		{
			for (int i = 0; i < m_rules.known_next_pieces; ++i)
				p.state.next[i] = m_randomPieces[p.next_piece_index++];

			p.state.hold = m_randomPieces[p.next_piece_index++];
		}

		m_players.push_back(p);
		m_connections.push_back(p.client);
		m_playerOverview[p.id] = { p.id, p.name, p.state.total_cleared_lines, p.state.score };
	}

	return true;
}

//---------------------------------------------------------------------------------------------------------------------
void game_server::wait_for_end_of_game()
{
	m_thread->join();
	m_thread.reset();
}

//---------------------------------------------------------------------------------------------------------------------
bool game_server::tick()
{
	for (auto &p : m_players)
	{
		auto state = p.state.serialize();
		p.client->write(state);
	}

	for (auto i = m_players.begin(); i != m_players.end(); )
	{
		if (i->state.playfield.game_over())
		{
			m_winners.push_back(i->id);
			i->client->write("QUIT");

			i = m_players.erase(i);
		}
		else
			++i;
	}

	for (auto &p : m_players)
	{
		auto actions = p.client->read();
		handle_player_actions(p, actions);

		m_playerOverview[p.id].cleared_lines = p.state.total_cleared_lines;
		m_playerOverview[p.id].score = p.state.score;
	}

	if (m_rules.send_garbage)
	{
		for (auto &p : m_players)
		{
			if (p.state.previous_cleared_lines == 4)
			{
				for (auto &other : m_players)
				{
					if (other.id != p.id)
					{
						auto garbage = m_randomGarbage[other.next_garbage_index++];
						other.next_garbage_index = other.next_garbage_index % m_randomGarbage.size();

						other.state.playfield.garbage(garbage);
					}
				}
			}
		}
	}

	if (m_rules.last_man_standing && m_players.size() == 1)
	{
		m_winners.push_back(m_players[0].id);
		m_players.clear();
	}

	if ((m_visualizeInterval > 0 && utils::zero_mod(m_tickCount, m_visualizeInterval)) || m_players.empty())
		visualize(m_visualizeMode);

	m_tickCount += 1;

	return !m_players.empty();
}

//---------------------------------------------------------------------------------------------------------------------
void game_server::handle_player_actions(player &p, std::string_view actions)
{
	auto newState = p.state;
	newState.play_actions(actions);

	// Can we go down one step?
	if (!newState.play_action(action::move_down))
	{
		newState.bake();
		newState.score += 1 + game_state::calculate_reward(newState.previous_cleared_lines);
		newState.previous_cleared_lines = newState.playfield.clear_lines();
		newState.total_cleared_lines += newState.previous_cleared_lines;
		newState.tick_count = m_tickCount;

		const auto &nextPiece = m_randomPieces[p.next_piece_index++];
		p.next_piece_index = p.next_piece_index % m_randomPieces.size();

		if (m_rules.known_next_pieces > 0)
		{
			newState.use_next_piece();
			newState.next[m_rules.known_next_pieces - 1] = nextPiece;
		}
		else
		{
			newState.next[0] = nextPiece;
			newState.use_next_piece();
		}

		if (m_rules.height_reduction > 0 && utils::zero_mod(p.next_piece_index, m_rules.height_reduction))
			newState.playfield.reduce_height();
	}

	p.state = newState;
}

//---------------------------------------------------------------------------------------------------------------------
void game_server::visualize(int mode) const
{
	if (mode <= 0)
		return;

	int x = 2;

	for (size_t i = 0; i < m_players.size(); ++i)
		x += m_players[i].state.print(x, 2, mode);

	// TODO
	/*
	if (m_rules.visualize_next)
	{
		for (size_t j = 0; j < rules::max_known_next_pieces + 1; ++j)
		{
			if (!j)
			{
				m_players[i].state.print_next(m_players[i].state.current, true, 2 + i * (15 + offset_for_next_piece) + 12 + (j % 2 * 6), 2 + (j / 2 * 6));
			}
			else
			{
				m_players[i].state.print_next(m_players[i].state.next[j - 1], false, 2 + i * (15 + offset_for_next_piece) + 12 + (j % 2 * 6), 2 + (j / 2 * 6));
			}
		}
	}
		*/
}

//---------------------------------------------------------------------------------------------------------------------
std::string game_server::overview_json()
{
	std::stringstream ss;
	int winner_id = -1;
	int winner_score = -1;

	ss << "{" << std::endl;
	ss << "\toverview: [ {" << std::endl;

	for (auto overviewIt = m_playerOverview.begin(); overviewIt != m_playerOverview.end();)
	{
		if (overviewIt->second.score > winner_score)
			winner_id = overviewIt->second.player_id;

		ss << "\t\tplayer_id: " << overviewIt->second.player_id << "," << std::endl;
		ss << "\t\tplayer_name: \"" << overviewIt->second.player_name << "\"," << std::endl;
		ss << "\t\tcleared_lines: " << overviewIt->second.cleared_lines << "," << std::endl;
		ss << "\t\tscore: " << overviewIt->second.score << std::endl;
		ss << "\t}";

		if ((++overviewIt) != m_playerOverview.end())
		{
			ss << ", {" << std::endl;
		}
	}

	ss << " ]," << std::endl;

	ss << "\telapsed_ms: " << std::chrono::duration_cast<std::chrono::milliseconds>(m_gameEnd - m_gameStart).count() << "," << std::endl;
	ss << "\tticks: " << m_tickCount << "," << std::endl;
	ss << "\twinner_id: " << winner_id << std::endl;
	ss << "}" << std::endl;

	return ss.str();
}

}
