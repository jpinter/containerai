#include <tetrisai.hpp>
#include <math.h>

using namespace tetrisai;

/*

Based on:
https://codemyroad.wordpress.com/2013/04/14/tetris-ai-the-near-perfect-player/

... but you can do better!

*/

//---------------------------------------------------------------------------------------------------------------------
float evaluateBoard(board b)
{
	float score = pow((1.0f + b.clear_lines()), 2.0f) * 1000.0f;

	// Calculate aggregated height and bumpiness
	int aggregateHeight = 0;
	int bumpiness = 0;

	int lastHeight = 0;
	for (int x = 0, SX = b.width(); x < SX; ++x)
	{
		for (int y = 0, SY = b.height(); y < SY; ++y)
		{
			if (b.at(x, y))
			{
				int height = SY - y;
				aggregateHeight += height;
				bumpiness += abs(height - lastHeight);
				lastHeight = height;
				break;
			}
		}
	}

	// Holes
	int holes = 0;
	for (int y = 0, SY = b.height(); y < SY; ++y)
	{
		for (int x = 0, SX = b.width(); x < SX; ++x)
		{
			if (!b.at(x, y))
			{
				if (b.at(x - 1, y) && b.at(x + 1, y) && b.at(x, y - 1))
					++holes;
			}
		}
	}

	score -= pow(static_cast<float>(aggregateHeight), 2.0f) * 100.0f;
	score -= pow(static_cast<float>(bumpiness), 2.0f) * 150.0f;
	score -= pow(static_cast<float>(holes), 2.0f) * 200.0f;

	return score;
}

//---------------------------------------------------------------------------------------------------------------------
struct placement_result
{
	float score = -std::numeric_limits<float>::infinity();
	int pos_x;
	int pos_y;
	int rotations;

	std::string to_actions(const game_state &s)
	{
		std::string result = "";

		int numSideMoves = abs(s.pos_x - pos_x);
		auto sideAction = (s.pos_x < pos_x)
			? action::move_right
			: action::move_left;

		for (int i = 0; i < rotations; ++i)
			result += static_cast<char>(action::rotate_left);

		for (int i = 0; i < numSideMoves; ++i)
			result += static_cast<char>(sideAction);

		result += static_cast<char>(action::fall_down);
		return result;
	}
};

//---------------------------------------------------------------------------------------------------------------------
placement_result findBestPlacement(const game_state &s)
{
	placement_result result;

	// Try all 4 rotations
	for (int r = 0, SR = s.current.rotation_count(); r < SR; ++r)
	{
		// Make a working copy of current game state
		game_state tmpState = s;

		// Rotate 'r' times
		for (int i = 0; i < r; ++i)
			tmpState.play_action(action::rotate_left);

		// Move current piece to the left as much as possible
		while (tmpState.play_action(action::move_left)) {}

		// Evaluate and then try moving right
		do
		{
			game_state fallState = tmpState;
			fallState.play_action(action::fall_down);
			fallState.bake();

			float score = 0.0f;

			if (fallState.use_next_piece())
				score = findBestPlacement(fallState).score;
			else
				score = evaluateBoard(fallState.playfield);

			if (score > result.score)
			{
				result.score = score;
				result.pos_x = tmpState.pos_x;
				result.pos_y = tmpState.pos_y;
				result.rotations = r;
			}

		} while (tmpState.play_action(action::move_right));

		// Let the piece fall
		tmpState.play_action(action::fall_down);
	}

	return result;
}

//---------------------------------------------------------------------------------------------------------------------
int main(int argc, char *argv[])
{
	cmd_line cmd(argc, argv);

	game_server server(cmd, 1);
	game_client client(cmd, "Example AI");

	try
	{
		while (client.wait_for_tick())
		{
			auto result = findBestPlacement(client.state());
			auto actions = result.to_actions(client.state());

			client.send_actions(actions);
		}
	}
	catch (const std::exception& e)
	{
		std::cerr << "Exception [exampleai]: " << e.what() << std::endl;
	}

	return 0;
}
