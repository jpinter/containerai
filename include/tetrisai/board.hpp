#pragma once

#include "piece.hpp"
#include <string.h>

namespace tetrisai {

class board
{
public:
	static constexpr int vertical_margin = 4;

	board();

	board(const board &copy) = default;
	board &operator=(const board &rhs) = default;

	// Board width (fixed)
	constexpr int width() const { return 10; }

	// Board height, can be changed during gameplay (see reduce_height & rules)
	int height() const { return m_height; }

	// Returns true, when cell at [x, y] is filled
	bool at(int x, int y) const;
	int at_int(int x, int y) const { return at(x, y) ? 1 : 0; }

	// Tests, if piece collides with this board at [x, y]
	bool test_collision(const piece &p, int x, int y) const;

	// Bakes/writes piece into board at [x, y]
	void bake(const piece &p, int x, int y);

	// Inserts garbage line from below
	void garbage(uint16_t garbageBits);

	// Reduces board height
	void reduce_height();

	// Calculates number of full filled lines (does NOT remove them)
	int full_lines_count() const;

	// Counts and clears all filled lines
	int clear_lines();

	// True, when anything reaches above allowed space
	bool game_over() const;

	std::string serialize() const;
	void deserialize(const std::string &msg);

protected:
	static constexpr int horizontal_margin = 3;

	static constexpr uint16_t side_walls = 0xE007u;
	static constexpr uint16_t filled_line = 0xFFFFu;
	static constexpr int lines_count = constants::board_height + 2 * vertical_margin;

	uint16_t m_lines[lines_count];

	int m_height = constants::board_height;
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//---------------------------------------------------------------------------------------------------------------------
board::board()
{
	int line = 0;

	for (int y = 0; y < constants::board_height + vertical_margin; ++y)
		m_lines[line++] = side_walls;

	for (int y = 0; y < vertical_margin; ++y)
		m_lines[line++] = filled_line;
}

//---------------------------------------------------------------------------------------------------------------------
bool board::at(int x, int y) const
{
	if (x < 0 || x >= width() || y < -vertical_margin || y >= m_height)
		return true;

	return m_lines[y + vertical_margin] & (1u << (x + horizontal_margin));
}

//---------------------------------------------------------------------------------------------------------------------
bool board::test_collision(const piece &p, int x, int y) const
{
	if (x < -horizontal_margin || x >= (width() + horizontal_margin) || y < -vertical_margin || y >= m_height)
		return true;

	auto pieceCells = static_cast<uint16_t>(p.data[0].to_ulong());

	x += horizontal_margin;
	y += vertical_margin;

	for (int py = 0; py < 4; ++py, ++y, pieceCells >>= 4)
		if (((pieceCells & 0xFu) << x) & m_lines[y])
			return true;

	return false;
}

//---------------------------------------------------------------------------------------------------------------------
void board::bake(const piece &p, int x, int y)
{
	if (x < -horizontal_margin || x >= (width() + horizontal_margin) || y < -vertical_margin || y >= m_height)
		return;

	auto pieceCells = static_cast<uint16_t>(p.data[0].to_ulong());

	x += horizontal_margin;
	y += vertical_margin;

	for (int py = 0; py < 4; ++py, ++y, pieceCells >>= 4)
		m_lines[y] |= (pieceCells & 0xFu) << x;
}

//---------------------------------------------------------------------------------------------------------------------
void board::garbage(uint16_t garbageBits)
{
	for (int y = 0; y < height() + vertical_margin - 1; ++y)
		m_lines[y] = m_lines[y + 1];

	m_lines[height() + vertical_margin - 1] = side_walls | (garbageBits << horizontal_margin);
}

//---------------------------------------------------------------------------------------------------------------------
void board::reduce_height()
{
	if (m_height <= 0)
		return;
	
	m_height -= 1;
	for (int y = 0; y < height() + vertical_margin; ++y)
		m_lines[y] = m_lines[y + 1];

	m_lines[height() + vertical_margin] = filled_line;
}

//---------------------------------------------------------------------------------------------------------------------
int board::full_lines_count() const
{
	int result = 0;

	for (int y = vertical_margin; y < height() + vertical_margin; ++y)
	{
		if (m_lines[y] == filled_line)
			result += 1;
	}

	return result;
}

//---------------------------------------------------------------------------------------------------------------------
int board::clear_lines()
{
	int clearedLines = 0;

	for (int y = vertical_margin; y < height() + vertical_margin; ++y)
	{
		if (m_lines[y] == filled_line)
		{
			memmove(&m_lines[1], &m_lines[0], sizeof(uint16_t) * y);
			clearedLines += 1;
		}
	}

	return clearedLines;
}

//---------------------------------------------------------------------------------------------------------------------
bool board::game_over() const
{
	for (int y = 0; y < 4; ++y)
		if (m_lines[y] != side_walls)
			return true;

	return false;
}

//---------------------------------------------------------------------------------------------------------------------
std::string board::serialize() const
{
	std::string result = std::to_string(m_height);

	for (auto line : m_lines)
		result += "|" + std::to_string(line);

	return result;
}

//---------------------------------------------------------------------------------------------------------------------
void board::deserialize(const std::string &msg)
{
	auto splitted = utils::split_string(msg, '|');
	m_height = std::stoi(splitted[0]);

	for (int i = 0; i < lines_count; ++i)
		m_lines[i] = std::stoi(splitted[i + 1]);
}

} // namespace tetrisai
