#pragma once

#include <array>
#include <bitset>
#include <chrono>
#include <cstdio>
#include <iostream>
#include <map>
#include <memory>
#include <sstream>
#include <string>
#include <string_view>
#include <thread>
#include <vector>

#if defined(_WIN32)
#if defined(_MSC_VER)
#pragma comment(lib, "ws2_32.lib")
#endif
#include <WinSock2.h>
#include <Windows.h>
#include <ws2tcpip.h>
#else
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/ip.h>
#include <unistd.h>
#include <netdb.h>
#endif

namespace tetrisai {

// Compile-time constants which can be safely changed
namespace constants {

// Default board height
static constexpr int board_height = 20;

// Maximum known number of next pieces ('known_next_pieces' command line argument cannot be larger than this)
static constexpr int max_known_next_pieces = 2;

}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

namespace detail {

//---------------------------------------------------------------------------------------------------------------------
struct no_case_comparator
{
	bool operator()(std::string_view s1, std::string_view s2) const
	{
		return std::lexicographical_compare(s1.begin(), s1.end(), s2.begin(), s2.end(), [](char c1, char c2)->bool
		{
			return tolower(c1) < tolower(c2);
		});
	}
};

}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

namespace utils {

//---------------------------------------------------------------------------------------------------------------------
template <typename T1, typename T2>
bool zero_mod(T1 dividend, T2 divisor)
{
	return (divisor > 0)
		? ((dividend % static_cast<T1>(divisor)) == 0)
		: false;
}

//---------------------------------------------------------------------------------------------------------------------
static std::vector<std::string> split_string(const std::string &str, char delimiter)
{
	std::vector<std::string> result;
	std::size_t current, previous = 0;
	current = str.find(delimiter);

	while (current != std::string::npos)
	{
		result.push_back(str.substr(previous, current - previous));
		previous = current + 1;
		current = str.find(delimiter, previous);
	}

	result.push_back(str.substr(previous, current - previous));

	return result;
}

}

} // namespace tetrisai
