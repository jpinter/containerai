#include <tetrisai.hpp>
#include <queue>
#include <cassert>
#include <random>

// thanks for Windows.h
#undef max

using namespace tetrisai;

// make use of hold feature
constexpr bool use_hold = true;
// add some (predictable) variety if evaluation would be the same
constexpr bool use_random = true;

// weights

constexpr int NUM_WEIGHTS = 7;

constexpr double weights[NUM_WEIGHTS] =
{
	0.12549, 0.0509804, 0.807843, 0.329412, 0.576471, 0.462745, 0.894118
};

constexpr int BORDER = 4;

//---------------------------------------------------------------------------------------------------------------------
double evaluate_board(board b, int pheight)
{
	b.clear_lines();

	if (b.game_over())
		return -INFINITY;

	const int W = b.width();
	const int H = b.height();

	double heightSum = 0;
	double maxHeight = 0;
	double holes = 0;
	double hswaps = 0;
	double vswaps = 0;
	double topology = 0;
	double topology2 = 0;
	int oheight = 0;

	const double *w = weights;

	for (int x = 0; x < W; x++)
	{
		// evaluate each column separately
		int height = 0;

		for (int y = H - 1; y >= 0; y--)
		{
			auto cur = b.at(x, y);
			auto prevx = b.at(x-1, y);

			hswaps += prevx != cur;
			vswaps += b.at(x, y-1) != cur;

			if (cur)
				height = H-y;
			else
			{
				if (prevx && b.at(x+1, y))
					topology2++;
			}
		}

		for (int y = 0; y<height; y++)
			holes += !b.at(x, H-1-y);

		int to_die = H-1-height;

		double nheight = to_die * weights[0];

		maxHeight = std::max(nheight, maxHeight);
		heightSum += nheight;

		if (x)
			topology += std::abs(height - oheight);

		oheight = height;
	}

	// finalize swaps
	for (int y = 0; y<H; y++)
		hswaps += b.at(W-1, y) != b.at(W, y);

	for (int x = 0; x<W; x++)
		vswaps += b.at(x, H-1) != b.at(x, H);

	double res = 0;

	++w;

	const auto applyWeight = [&w, &res](double weight, double scale)
	{
		double tmp = weight * *w++;
		res -= tmp*scale;
	};

	applyWeight(topology, 1);
	applyWeight(holes, 1);
	applyWeight((double)pheight, -1);
	applyWeight(topology2, 1);
	applyWeight(hswaps, 1);
	applyWeight(vswaps, 1);
	res += heightSum;

	return res;
}

//---------------------------------------------------------------------------------------------------------------------
char extract_path(int pos_x, int pos_y, int prev_x, int prev_y)
{
	assert((pos_y == prev_y && abs(pos_x - prev_x) == 1) || (pos_x == prev_x && pos_y == prev_y+1));

	if (pos_y > prev_y)
		return (char)action::move_down;

	return char(pos_x < prev_x ? action::move_left : action::move_right);
}

//---------------------------------------------------------------------------------------------------------------------
std::vector<std::string> dijkstra(const game_state &s, bool fast = true)
{
	struct tile
	{
		// distance to origin
		int distance = INT_MAX;
		// previous tile
		short prev_x = 0;
		short prev_y = 0;
		short terminal = 0;
		short processed = 0;
	};

	struct elem
	{
		// distance to origin
		int distance;
		// position
		short pos_x;
		short pos_y;

		bool operator <(const elem &o) const
		{
			return distance < o.distance;
		}
	};

	struct dir
	{
		int x, y;
	};

	auto tmp = s;

	std::vector<tile> tiles;
	// terminal positions (can't move down)
	std::vector<int> terminal;

	const int width = s.playfield.width() + 2*BORDER;
	const int height = s.playfield.height() + 2*BORDER;

	const auto get_tile = [&](int x, int y, int &ofs)->tile&
	{
		ofs = (y + BORDER) * width + (x + BORDER);
		return tiles[ofs];
	};

	int ofs;

	tiles.resize(width*height);

	std::priority_queue<elem, std::vector<elem>, std::less<elem>> pq;

	pq.push(elem {0, (short)s.pos_x, (short)s.pos_y});

	auto &st = get_tile(s.pos_x, s.pos_y, ofs);
	st.distance = 0;

	const std::array<action, 3> actions =
	{
		action::move_left,
		action::move_right,
		action::move_down
	};

	while (!pq.empty())
	{
		auto e = pq.top();
		pq.pop();

		auto &et = get_tile(e.pos_x, e.pos_y, ofs);

		if (et.distance != e.distance)
			continue;

		et.processed = 1;

		// try all moves: left, right, down
		for (auto it : actions)
		{
			tmp.pos_x = e.pos_x;
			tmp.pos_y = e.pos_y;

			if (!tmp.play_action(it))
			{
				if (it == action::move_down)
				{
					auto &t = get_tile(e.pos_x, e.pos_y, ofs);

					if (!t.terminal)
					{
						t.terminal = 1;
						terminal.push_back(ofs);
					}
				}
				continue;
			}

			auto &t = get_tile(tmp.pos_x, tmp.pos_y, ofs);

			if (fast && t.processed)
				continue;

			const auto new_dist = e.distance+1;

			if (new_dist < t.distance)
			{
				t.distance = new_dist;
				t.prev_x = e.pos_x;
				t.prev_y = e.pos_y;

				pq.push(elem{new_dist, (short)tmp.pos_x, (short)tmp.pos_y});
			}
		}

		tmp.pos_x = e.pos_x;
		tmp.pos_y = e.pos_y;
	}

	std::vector<std::string> moves;

	const bool can_hold = use_hold && s.next[0].valid();

	const std::array<action, 2> rotations =
	{
		action::rotate_left,
		action::rotate_right
	};

	// okay so we have a list of terminal nodes, now extract paths as strings AND generate moves
	for (auto ofs : terminal)
	{
		// unpack offset
		int x = ofs % width;
		int y = ofs / width;
		int pos_x = x - BORDER;
		int pos_y = y - BORDER;

		// extract path
		std::string path;

		const auto *t = &tiles[ofs];

		while (t->distance > 0)
		{
			path += extract_path(pos_x, pos_y, t->prev_x, t->prev_y);

			pos_x = t->prev_x;
			pos_y = t->prev_y;

			ofs = (pos_y + BORDER)*width + (pos_x + BORDER);
			t = &tiles[ofs];
		}

		std::reverse(path.begin(), path.end());

		// compress path (sequence of move_down => fall_down)
		bool compress = false;

		while (!path.empty() && path.back() == (char)action::move_down)
		{
			compress = true;
			path.pop_back();
		}

		if (compress)
			path += (char)action::fall_down;

		moves.push_back(path);

		if (can_hold)
			moves.push_back(path + (char)action::hold);

		// try T-spin (rot left/right)
		auto old = tmp.current;

		pos_x = x - BORDER;
		pos_y = y - BORDER;

		for (auto it : rotations)
		{
			tmp.pos_x = pos_x;
			tmp.pos_y = pos_y;
			tmp.current = old;

			if (!tmp.play_action(it))
				continue;

			tmp.current = old;

			auto spath = path;
			spath += (char)it;
			spath += (char)action::fall_down;
			moves.push_back(spath);

			if (can_hold)
				moves.push_back(spath + (char)action::hold);
		}
	}

	return moves;
}

//---------------------------------------------------------------------------------------------------------------------
// returns number of moves
int gen_moves(const game_state &s, std::vector<std::string> &moves, bool fast)
{
	moves.clear();

	const int nrot = s.current.rotation_count();

	for (int r = 0; r<nrot; r++)
	{
		auto tmp = s;

		int nr = r;

		std::string prefix;

		while (nr != 0)
		{
			const auto act = nr < 0 ? action::rotate_left : action::rotate_right;

			if (!tmp.play_action(act))
				break;

			prefix += char(act);
			nr += (nr < 0) ? 1 : -1;
		}

		if (nr)
			continue;

		auto dmoves = dijkstra(tmp, fast);

		for (auto &&it : dmoves)
			moves.emplace_back(std::move(prefix + it));
	}

	thread_local std::mt19937 rng(1234);

	if (use_random)
		std::shuffle(moves.begin(), moves.end(), rng);

	return (int)moves.size();
}

//---------------------------------------------------------------------------------------------------------------------
double evaluate_leaf(const game_state &s, int pheight)
{
	double res = evaluate_board(s.playfield, pheight);

	auto next = s;

	if (!next.use_next_piece() || next.playfield.game_over())
		return res;

	std::vector<std::string> moves;
	const int move_count = gen_moves(next, moves, true);

	if (!move_count)
		return res;

	double best = -INFINITY;

	for (int i=0; i<move_count; i++)
	{
		auto tmp = next;

		if (tmp.play_actions(moves[i]) != moves[i].length())
		{
			// should never happen!
			continue;
		}

		int pheight = std::max<int>(tmp.pos_y, 0);
		tmp.bake();

		auto val = evaluate_leaf(tmp, pheight);

		if (val > best)
			best = val;
	}

	return best;
}

//---------------------------------------------------------------------------------------------------------------------
std::string think(const game_state &s)
{
	// we generate all the moves here
	std::string result;

	std::vector<std::string> moves;
	const int move_count = gen_moves(s, moves, false);

	double best = -INFINITY;
	int best_move = -1;

	for (int i=0; i<move_count; i++)
	{
		auto tmp = s;

		if (tmp.play_actions(moves[i]) != moves[i].length())
		{
			// should never happen!
			continue;
		}

		int pheight = std::max<int>(tmp.pos_y, 0);
		tmp.bake();

		// force 1 lookahead only, 2 would be very slow
		tmp.next[1] = piece();

		auto val = evaluate_leaf(tmp, pheight);

		if (val > best)
		{
			best = val;
			best_move = i;
		}
	}

	if (best_move >= 0)
		result = std::move(moves[best_move]);

	return result;
}

//---------------------------------------------------------------------------------------------------------------------
int main(int argc, char *argv[])
{
	cmd_line cmd(argc, argv);

	game_server server(cmd, 1);
	game_client client(cmd, "T'Kuzhma v3");

	while (client.wait_for_tick())
		client.send_actions(think(client.state()));

	return 0;
}
