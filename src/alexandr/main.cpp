#include <tetrisai.hpp>
#include <thread>

using namespace tetrisai;

//---------------------------------------------------------------------------------------------------------------------
void print_help()
{
	std::cerr << "Soudce Alexandr v0.0.1" << std::endl;
	std::cerr << std::endl;
	std::cerr << "Usage:" << std::endl;
	std::cerr << "  alexandr AI1 AI2 AI3... [-options...]" << std::endl;
	std::cerr << std::endl;
	std::cerr << "Options:" << std::endl;
	std::cerr << "  -help: exactly this!" << std::endl;
	std::cerr << "  -port=X: listen on port X (default is " << net::default_server_port << ")" << std::endl;
	std::cerr << "  -visualize_mode=X: 0=off, 1=board, 2=board+pieces" << std::endl;
	std::cerr << "  -visualize_interval=X: redraw every X ticks" << std::endl;
	std::cerr << "  -last_man_standing: last living AI wins immediately" << std::endl;
	std::cerr << "  -send_garbage: add garbage line to opponents on tetris" << std::endl;
	std::cerr << "  -known_next_pieces=X: AI knows X upcomming pieces" << std::endl;
	std::cerr << "  -height_reduction=X: reduce board height every X pieces" << std::endl;
	std::cerr << "  -extended_piece_set: use uncommon tetrominos (EXTRA HARD!)" << std::endl;
	std::cerr << "  -max_game_time=X: force end of the game after X ms" << std::endl;
	std::cerr << "  -max_tick_time=X: force end of the game when player won't react for X ms" << std::endl;
	std::cerr << std::endl;
	std::cerr << "Example:" << std::endl;
	std::cerr << "  alexandr exampleai hal9000 -visualize -send_garbage -height_reduction=1000" << std::endl;
}

//---------------------------------------------------------------------------------------------------------------------
void run_ai(const char *name, const cmd_line &cmd)
{
	using namespace std::string_literals;

	std::string nameWithArgs =
#ifndef _WIN32
		"./"s +
#endif
		name;

	nameWithArgs += " ";
	nameWithArgs += cmd.to_string();

	system(nameWithArgs.c_str());
}

//---------------------------------------------------------------------------------------------------------------------
int main(int argc, char *argv[])
{
	cmd_line cmd(argc, argv);
	if (cmd.has_option("help"))
	{
		print_help();
		return 0;
	}

	// Inject some fake args...
	// cmd.values.push_back("exampleai");
	// cmd.values.push_back("exampleai");

	if (cmd.values.empty())
	{
		std::cerr << "No AI players specified!" << std::endl;
		std::cerr << "Use -help..." << std::endl;
		return -1;
	}

	game_server server(cmd, static_cast<int>(cmd.values.size()));

	cmd.options["server"] =
		std::string("127.0.0.1:") +
		std::to_string(cmd.option_int("port", net::default_server_port));

	std::vector<std::thread> aiThreads;

	for (auto &value : cmd.values)
		aiThreads.emplace_back(run_ai, value.c_str(), cmd);

	server.wait_for_end_of_game();

	for (auto &t : aiThreads)
		t.join();

	std::cout << server.overview_json();
	std::getchar();

	aiThreads.clear();
	return 0;
}
