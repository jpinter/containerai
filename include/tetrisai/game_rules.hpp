#pragma once

#include "cmd_line.hpp"
#include "piece.hpp"

namespace tetrisai {

struct game_rules
{
	bool last_man_standing = false;
	bool send_garbage = false;
	int known_next_pieces = 1;
	int height_reduction = 0;
	bool extended_piece_set = false;
	int max_game_time = 60000; // 1 minute per game
	int max_tick_time = 1000; // 1 second per tick

	game_rules() = default;
	game_rules(const cmd_line &cmd);

	void sanitize();
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//---------------------------------------------------------------------------------------------------------------------
game_rules::game_rules(const cmd_line &cmd)
{
	last_man_standing = cmd.has_option("last_man_standing");
	send_garbage = cmd.has_option("send_garbage");
	known_next_pieces = cmd.option_int("known_next_pieces", 1);
	height_reduction = cmd.option_int("height_reduction", 0);
	extended_piece_set = cmd.has_option("extended_piece_set");
	max_game_time = cmd.option_int("max_game_time", 60000);
	max_tick_time = cmd.option_int("max_tick_time", 1000);

	sanitize();
}

//---------------------------------------------------------------------------------------------------------------------
void game_rules::sanitize()
{
	if (known_next_pieces > constants::max_known_next_pieces)
		known_next_pieces = constants::max_known_next_pieces;
}

}
