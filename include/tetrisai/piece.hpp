#pragma once

#include "base.hpp"

namespace tetrisai {

struct piece
{
	static constexpr int standard_piece_count = 7;
	static constexpr int extended_piece_count = 12;

	// 16 bits representing 4x4 rotation variation
	using cells = std::bitset<16>;

	// All 4 rotations in clockwise order, data[0] is always the main/current one
	using rotations = std::array<cells, 4>;
	rotations data;

	// Packs 4x4 grid of 'X' and ' ' into 16 bits
	static cells cells_from_string(std::string_view dataStr);

	// Creates a piece from type ID
	static piece create(int type);

	piece(const rotations &rots)
		: data(rots)
	{ }

	// Default ctor, creates invalid (empty) piece.
	piece()
		: piece(rotations({ 0, 0, 0, 0 }))
	{ }

	// Piece with two repeating rotations
	piece(const cells &first, const cells &second)
		: piece(rotations({ first, second, first, second }))
	{ }

	// Piece with one single rotation
	piece(const cells &allTheSame)
		: piece(rotations({ allTheSame, allTheSame, allTheSame, allTheSame }))
	{ }

	// Returns true, when cell at [x, y] is filled
	bool at(int x, int y) const;

	// Returns new rotated piece
	piece rotate(bool clockwise = true) const;

	// Calculates number of non-repeating rotation operations
	int rotation_count() const;

	// Checks, if piece is valid or empty/invalid (see default ctor)
	bool valid() const { return data[0].any(); }

	std::string serialize() const;
	void deserialize(const std::string &msg);
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//---------------------------------------------------------------------------------------------------------------------
piece::cells piece::cells_from_string(std::string_view dataStr)
{
	return cells(dataStr.data(), dataStr.size(), ' ', 'X');
}

//---------------------------------------------------------------------------------------------------------------------
piece piece::create(int type)
{
	switch (type)
	{
		case 0:
			return piece(
				cells_from_string(
					" XX "
					" XX "
					"    "
					"    "
				)
			);

		case 1:
			return piece(
				cells_from_string(
					"    "
					"XXXX"
					"    "
					"    "
				),
				cells_from_string(
					" X  "
					" X  "
					" X  "
					" X  "
				)
			);

		case 2:
			return piece(
				cells_from_string(
					" XX "
					"XX  "
					"    "
					"    "
				),
				cells_from_string(
					"X   "
					"XX  "
					" X  "
					"    "
				)
			);

		case 3:
			return piece(
				cells_from_string(
					"XX  "
					" XX "
					"    "
					"    "
				),
				cells_from_string(
					"  X "
					" XX "
					" X  "
					"    "
				)
			);

		case 4:
			return piece({
				cells_from_string(
					"X   "
					"X   "
					"XX  "
					"    "
				),
				cells_from_string(
					"XXX "
					"X   "
					"    "
					"    "
				),
				cells_from_string(
					" XX "
					"  X "
					"  X "
					"    "
				),
				cells_from_string(
					"    "
					"  X "
					"XXX "
					"    "
				)
			});

		case 5:
			return piece({
				cells_from_string(
					"  X "
					"  X "
					" XX "
					"    "
				),
				cells_from_string(
					"    "
					"X   "
					"XXX "
					"    "
				),
				cells_from_string(
					"XX  "
					"X   "
					"X   "
					"    "
				),
				cells_from_string(
					"XXX "
					"  X "
					"    "
					"    "
				)
				});

		case 6:
			return piece({
				cells_from_string(
					"    "
					"XXX "
					" X  "
					"    "
				),
				cells_from_string(
					" X  "
					"XX  "
					" X  "
					"    "
				),
				cells_from_string(
					" X  "
					"XXX "
					"    "
					"    "
				),
				cells_from_string(
					" X  "
					" XX "
					" X  "
					"    "
				)
				});

		case 7:
			return piece({
				cells_from_string(
					"X X "
					"X X "
					"XXX "
					"    "
				),
				cells_from_string(
					"XXX "
					"X   "
					"XXX "
					"    "
				),
				cells_from_string(
					"XXX "
					"X X "
					"X X "
					"    "
				),
				cells_from_string(
					"XXX "
					"  X "
					"XXX "
					"    "
				)
				});

		case 8:
			return piece({
				cells_from_string(
					"XXX "
					" X  "
					" X  "
					"    "
				),
				cells_from_string(
					"  X "
					"XXX "
					"  X "
					"    "
				),
				cells_from_string(
					" X  "
					" X  "
					"XXX "
					"    "
				),
				cells_from_string(
					"X   "
					"XXX "
					"X   "
					"    "
				)
				});

		case 9:
			return piece({
				cells_from_string(
					"X   "
					"X   "
					"XXX "
					"    "
				),
				cells_from_string(
					"XXX "
					"X   "
					"X   "
					"    "
				),
				cells_from_string(
					"XXX "
					"  X "
					"  X "
					"    "
				),
				cells_from_string(
					"  X "
					"  X "
					"XXX "
					"    "
				)
				});

		case 10:
			return piece(
				cells_from_string(
					" XX "
					" X  "
					"XX  "
					"    "
				),
				cells_from_string(
					"X   "
					"XXX "
					"  X "
					"    "
				));

		case 11:
			return piece(
				cells_from_string(
					"XX  "
					" X  "
					" XX "
					"    "
				),
				cells_from_string(
					"  X "
					"XXX "
					"X   "
					"    "
				));

		default:
			throw std::runtime_error("Unknown piece type!");
	}
}

//---------------------------------------------------------------------------------------------------------------------
bool piece::at(int x, int y) const
{
	if (x < 0 || y < 0 || x >= 4 || y >= 4)
		return false;

	return data[0][y * 4 + x];
}

//---------------------------------------------------------------------------------------------------------------------
piece piece::rotate(bool clockwise) const
{
	return clockwise
		? piece({ data[1], data[2], data[3], data[0] })
		: piece({ data[3], data[0], data[1], data[2] });
}

//---------------------------------------------------------------------------------------------------------------------
int piece::rotation_count() const
{
	if (!valid())
		return 0;

	if (data[0] == data[1])
		return 1;
	else if (data[0] == data[2])
		return 2;

	return 4;
}

//---------------------------------------------------------------------------------------------------------------------
std::string piece::serialize() const
{
	uint64_t mergedData =
		data[0].to_ullong()       |
		data[1].to_ullong() << 16 |
		data[2].to_ullong() << 32 |
		data[3].to_ullong() << 48;

	return std::to_string(mergedData);
}

//---------------------------------------------------------------------------------------------------------------------
void piece::deserialize(const std::string &msg)
{
	uint64_t mergedData = std::stoull(msg);
	data = {
		static_cast<uint16_t>((mergedData)       & 0xffff),
		static_cast<uint16_t>((mergedData >> 16) & 0xffff),
		static_cast<uint16_t>((mergedData >> 32) & 0xffff),
		static_cast<uint16_t>((mergedData >> 48) & 0xffff),
	};
}

} // namespace tetrisai
