# ContainerAI

Tetris AI Challenge for Container Brno #6

## How does it work?
* client/server architecture (TCP socket)
    * allows multiplayer games
* server and clients run in a synchronous lockstep:
    * client receives state of the game (board, current piece, next piece, score, ...)
    * client evaluates the board and generates sequence of moves & rotations and sends it to the server
    * server evaluates, if those moves are valid and applies them to the state of the game
    * this repeats until end of the game (possible end game rules explained below)

## What should I do?
* instantiate `game_client`, connect to the server, and send moves - that's it, really!
* look at `src/yourai/main.cpp` for the stub
    * and make it your own :)
    * this is where you start development!
* look at `src/exampleai/main.cpp` for dummy AI, which can already play
    * take inspiration!
* you can run your AI as
    * a standalone executable
        * `./yourai`
        * in this case, the server & client is running within this single executable
    * using judge Alexandr
        * `./alexandr yourai`
        * in this case, the server is running in the `alexandr` and the client is `yourai`
    * using judge Alexandr with another player
        * `./alexandr yourai exampleai -known_next_pieces=0 -send_garbage`
        * runs `yourai` & `exampleai` against each other with specified game rules
        * `alexandr` will propagate command-line arguments to clients
        * yes, you can run one AI against itself! `./alexandr exampleai exampleai exampleai`

## How will be the game evaluated?
* singleplayer: `known_next_pieces=0 height_reduction=1000 max_game_time=60000 max_tick_time=1000`
* singleplayer: `known_next_pieces=1 height_reduction=1000 max_game_time=60000 max_tick_time=1000`
* multiplayer 1vs1: `known_next_pieces=0 send_garbage=1 last_man_standing=1 height_reduction=2000 max_game_time=60000 max_tick_time=1000`
* multiplayer 1vs1: `known_next_pieces=1 send_garbage=1 last_man_standing=1 height_reduction=2000 max_game_time=60000 max_tick_time=1000`
* (parameters are parsed by constructor of `game_client` & `game_server` automatically)
* each AI will be playing against each
* each game will be run only **once**
* explanation:
    * `known_next_pieces=0`: client won't know what piece will fall as next
    * `send_garbage=1`: when oponent clears 4 lines at once, your board will receive one "garbage" row at the bottom
    * `last_man_standing=1`: when oponent loses, the whole game is over
    * `height_reduction=2000`: after each 2000 pieces the player receives, the height of the board will be shrunk by 1 row
    * `max_game_time=60000`: when no player loses after 1 minute, the game will end (score will be kept)
    * `max_tick_time=1000`: when player won't send any action to the server after 1 second, the game will end; the game will end even in multiplayer mode (score will be kept for each player)
    * for more explanation of these parameters, see `struct rules` in this document
* **AI whose combined score in all 4 evaluations will be the highest is the winner**
    * detailed table of each evaluation will be published, of course :)

## Scoring
* done by `game_state::calculate_reward(int clearedLines)` method
* clearedLines^2 * 10
    * 1 cleared line: **+10**
    * 2 cleared lines: **+20**
    * 3 cleared lines: **+40**
    * 4 cleared lines **+80**

## What if...
* ... I make invalid moves?
    * Invalid moves (e.g. moving 50x to the left) are simply ignored. If your piece is located one column from the right side, and you send 50x right & 1 down, the piece will be moved by 1 column to the right & 1 row down.
* ... I wait indefinitelly in the tick?
    * Your current game will be over. Your score will be kept.
* ... I exploit some bug to win the game?
    * You will receive our admiration, but unfortunatelly, you will be disqualified :) (let us know about bugs you find, though!)

## Brief documentation 

* `class piece`
    * holds shape of the piece & its rotations (`std::bitset<16> piece::data`)
    * pregenerated (created via factory `piece::create(int type)`)
    * immutable
* `class board`
    * holds height, width (fixed at 10)
    * pieces are "baked" into board (`board::bake(const piece &p, int x, int y)`)
    * NOT immutable, but copyable
    * actual width is 16 columns, first 3 & last 3 columns are used as walls
        * test collision with `board::test_collision(const piece &p, int x, int y) const` 
    * actual height is _h + 8_
        * first 4 rows are used as spawning area for new pieces
        * last 4 rows are used as bedrock
* `class game_state`
    * holds board, score, current falling piece (not baked into board!) & its position (`pos_x`, `pos_y`), **held piece**, and next pieces to fall
    * NOT immutable, but copyable
* `enum action`
    * move_left, move_right, move_down: moves piece by 1 point in specified direction
    * rotate_left, rotate_right: rotates piece (counter)clockwise
    * fall_down: moves piece down until it finds collision at `y` coordinates
    * hold: swaps `game_state::next[0]` with `game_state::hold`. In other words, you can put next piece "on hold" and receive the held piece as a next one. Use it in your advance!

* `struct game_rules`
    * holds information about current game rules
    * rules are parsed from command-line arguments (see `game_rules::game_rules(const cmd_line &cmd)`)
    * last_man_standing: if true, when playing multiplayer, the game will end when all players reach _game over_ state; if false, the game will run until every single player won't end the game.
    * send_garbage: if true, when playing multiplayer, if a player clears 4 rows at once, other players receive random garbage line at the bottom
    * height_reduction: if > 0, the height of the `board` is reduced (from bottom, i.e. bedrock is increased)
    * known_pieces: number of known pieces. This number can be in range `<0, 4>`; when 0, you don't know which piece will fall next (**you can't use action::hold!**); when > 0, you can access next pieces at `game_state::next[n]`. `game_state::next[0]` is the very next falling piece
    * max_game_time: if there is no winner/loser after this amount of time (in milliseconds), the game will end
    * max_tick_time: if any player won't send any action to the server in this time (in milliseconds), the game will end; the timer is reset with each tick (i.e. after player receives `game_state` from the server, he has X-ms for the response)

* `class game_server`
    * creates TCP server socket
    * `game_server::wait_for_players(int count)` will wait for `count` players to connect, then runs the game in separate thread
    * `game_server::wait_for_end_of_game()` will block until all players finished playing

* `class game_client`
    * holds current state & rules (received from the `game_server`)
    * sends moves to the server (`game_client::send_moves(std::string_view moves)`)
    * waits for the server to respond with updated state (`game_client::wait_for_tick()`)
