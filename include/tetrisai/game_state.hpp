#pragma once

#include "board.hpp"

namespace tetrisai {

enum class action : char
{
	move_left = '<',
	move_right = '>',
	move_down = 'v',
	rotate_left = '(',
	rotate_right = ')',
	fall_down = '_',
	hold = '*',
};

struct game_state
{
	board playfield;

	int score = 0;
	int previous_cleared_lines = 0;
	int total_cleared_lines = 0;
	int tick_count = 0;

	int pos_x = 3;
	int pos_y = -board::vertical_margin;

	piece current;
	piece hold;
	piece next[constants::max_known_next_pieces];

	std::string serialize() const;
	void deserialize(const std::string &msg);

	static int calculate_reward(int clearedLines);

	bool use_next_piece();
	void bake();

	bool play_action(action move);
	int play_actions(std::string_view actions);

	int print(int x, int y, int mode) const;
	void print_next(const piece &p, bool is_current, int x, int y) const;
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//---------------------------------------------------------------------------------------------------------------------
std::string game_state::serialize() const
{
	std::string result =
		std::to_string(score)
		+ "," + std::to_string(pos_x)
		+ "," + std::to_string(pos_y)
		+ "," + playfield.serialize()
		+ "," + current.serialize()
		+ "," + hold.serialize();

	for (int i = 0; i < constants::max_known_next_pieces; ++i)
		result += "," + next[i].serialize();

	return result;
}

//---------------------------------------------------------------------------------------------------------------------
void game_state::deserialize(const std::string &msg)
{
	auto splitted = utils::split_string(msg, ',');

	score = std::stoi(splitted[0]);
	pos_x = std::stoi(splitted[1]);
	pos_y = std::stoi(splitted[2]);
	playfield.deserialize(splitted[3]);
	current.deserialize(splitted[4]);
	hold.deserialize(splitted[5]);

	for (int i = 0; i < constants::max_known_next_pieces; ++i)
		next[i].deserialize(splitted[6 + i]);
}

//---------------------------------------------------------------------------------------------------------------------
int game_state::calculate_reward(int clearedLines)
{
	return clearedLines > 0
		? (1 << (clearedLines - 1)) * 10
		: 0;
}

//---------------------------------------------------------------------------------------------------------------------
bool game_state::use_next_piece()
{
	if (!next[0].valid())
		return false;

	pos_x = (playfield.width() - 4) / 2;
	pos_y = -board::vertical_margin;

	current = next[0];

	for (int i = 0; i < constants::max_known_next_pieces - 1; ++i)
		next[i] = next[i + 1];

	next[constants::max_known_next_pieces - 1] = piece();
	return true;
}

//---------------------------------------------------------------------------------------------------------------------
void game_state::bake()
{
	playfield.bake(current, pos_x, pos_y);
}

//---------------------------------------------------------------------------------------------------------------------
bool game_state::play_action(action move)
{
	piece tmpPiece;

	switch (move)
	{
		case action::move_left:
			if (!playfield.test_collision(current, pos_x - 1, pos_y))
			{
				pos_x -= 1;
				return true;
			}
			break;

		case action::move_right:
			if (!playfield.test_collision(current, pos_x + 1, pos_y))
			{
				pos_x += 1;
				return true;
			}
			break;

		case action::move_down:
			if (!playfield.test_collision(current, pos_x, pos_y + 1))
			{
				pos_y += 1;
				return true;
			}
			break;

		case action::rotate_left:
			tmpPiece = current.rotate(false);
			if (!playfield.test_collision(tmpPiece, pos_x, pos_y))
			{
				current = tmpPiece;
				return true;
			}
			break;

		case action::rotate_right:
			tmpPiece = current.rotate(true);
			if (!playfield.test_collision(tmpPiece, pos_x, pos_y))
			{
				current = tmpPiece;
				return true;
			}
			break;

		case action::fall_down:
			tmpPiece = current;
			for (int tmpY = pos_y; ; ++tmpY)
			{
				if (playfield.test_collision(tmpPiece, pos_x, tmpY))
				{
					pos_y = tmpY - 1;
					return true;
				}
			}
			break;

		case action::hold:
			if (next[0].valid())
			{
				tmpPiece = hold;
				hold = next[0];
				next[0] = tmpPiece;
				return true;
			}
			break;

		default:
			break;
	}

	return false;
}

//---------------------------------------------------------------------------------------------------------------------
int game_state::play_actions(std::string_view actions)
{
	// Your moves are weak!
	int movesCount = 0;

	for (char a : actions)
	{
		if (play_action(static_cast<action>(a)))
		{
			// print(0, 0, 1); std::this_thread::sleep_for(std::chrono::milliseconds(500));
			movesCount += 1;
		}
	}

	// We are not fam anymore!
	return movesCount;
}

//---------------------------------------------------------------------------------------------------------------------
int game_state::print(int x, int y, int mode) const
{
#if defined(_WIN32)
	// Enable ANSI escape sequences in Windows console
	static HANDLE hStdOut = nullptr;
	if (!hStdOut)
	{
		hStdOut = CreateFileW(L"CONOUT$", GENERIC_WRITE, 0, nullptr, OPEN_EXISTING, 0, nullptr);
		SetConsoleMode(hStdOut, ENABLE_PROCESSED_OUTPUT | ENABLE_VIRTUAL_TERMINAL_PROCESSING);
	}
#endif

	int width = 15;

	auto p = playfield;
	p.bake(current, pos_x, pos_y);

	std::stringstream ss;

	int bgColor, lastBgColor = -1;

	for (int py = 0; py <= constants::board_height; ++py)
	{
		ss << "\033[" << (py + y) << ";" << x << "H";

		for (int px = -1; px < 11; ++px)
		{
			// empty space (default)
			bgColor = 40; // black

			// filled space
			if (p.at(px, py))
			{
				bgColor = 41; // red
			}

			// current piece
			if (current.at(px - pos_x, py - pos_y))
			{
				bgColor = 44; // blue
			}

			// additional height
			if (px >= 0 && px < 10 && py >= p.height())
			{
				bgColor = 47; // white
			}

			// side walls
			else if (px == -1 || px == 10 || py == 20)
			{
				bgColor = 47; // white
			}

			if (bgColor != lastBgColor)
			{
				lastBgColor = bgColor;
				ss << "\033[" << bgColor << "m";
			}

			ss << ' '; // fill with space
		}
	}

	ss << "\033[" << (y + 21) << ";" << x << "H";
	ss << "\033[0m"; // reset colors
	ss << "\033[" << 97 << "m";

	ss.fill(' ');
	ss.width(12);
	ss << score;

	std::cout << ss.str();
	return width;
}

//---------------------------------------------------------------------------------------------------------------------
void game_state::print_next(const piece &p, bool is_current, int x, int y) const
{
	std::stringstream ss;

	static constexpr int x_min = -1;
	static constexpr int y_min = -1;
	static constexpr int x_max = 5;
	static constexpr int y_max = 5;

	for (int py = y_min; py <= y_max; ++py)
	{
		ss << "\033[" << (py + y - y_min) << ";" << x << "H";

		for (int px = x_min; px <= x_max; ++px)
		{
			if (px == x_min || py == y_min || px == y_max || py == x_max)
			{
				ss << '\xb0';
			}
			else
			{
				if (is_current)
				{
					ss << "\033[" << 31 << "m";
				}
				ss << (p.at(px, py) ? '\xdb' : ' ');
				ss << "\033[" << 97 << "m";
			}
		}
	}

	ss << "\033[" << (y + 21) << ";" << x << "H";
	ss << "\033[" << 97 << "m";

	std::cerr << ss.str();
}

}
