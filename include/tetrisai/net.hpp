#pragma once

#include "base.hpp"

namespace tetrisai::net {

static constexpr int default_server_port = 20666;

/* Forward declarations */
class tcp_client;
class tcp_server;

namespace detail {
static constexpr uint32_t default_socket_timeout = 500;
#if defined(_WIN32)
using socket_type = SOCKET;
static constexpr int socket_error = SOCKET_ERROR;
static constexpr SOCKET invalid_socket = INVALID_SOCKET;
static void close_socket(socket_type s) { closesocket(s); }
static void set_socket_timeout(socket_type s, uint32_t milliseconds = default_socket_timeout)
{
	uint32_t tv = milliseconds;
	setsockopt(s, SOL_SOCKET, SO_RCVTIMEO, reinterpret_cast<const char*>(&tv), sizeof(uint32_t));
	setsockopt(s, SOL_SOCKET, SO_SNDTIMEO, reinterpret_cast<const char*>(&tv), sizeof(uint32_t));
}
#else
using socket_type = int;
static constexpr int socket_error = -1;
static constexpr int invalid_socket = -1;
static void close_socket(socket_type s) { close(s); }
static void set_socket_timeout(socket_type s, uint32_t milliseconds = default_socket_timeout)
{
	timeval tv;
	tv.tv_sec = (default_socket_timeout / 1000);
	tv.tv_usec = (default_socket_timeout % 1000) * 1000;
	setsockopt(s, SOL_SOCKET, SO_RCVTIMEO, reinterpret_cast<const void*>(&tv), sizeof(timeval));
	setsockopt(s, SOL_SOCKET, SO_SNDTIMEO, reinterpret_cast<const void*>(&tv), sizeof(timeval));
}
#endif
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class tcp_server
{
public:
	tcp_server(int port);

	~tcp_server();

	std::shared_ptr<tcp_client> accept_client();

protected:
	int m_port = 0;
	detail::socket_type m_socket = detail::invalid_socket;
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class tcp_client
{
public:
	tcp_client();

	tcp_client(detail::socket_type s, const sockaddr_in &from);

	~tcp_client();

	bool connect(std::string_view address, int port);

	bool is_connected() const { return m_socket != detail::invalid_socket; }

	void set_timeout(uint32_t timeout) { detail::set_socket_timeout(m_socket, timeout); }

	std::string read();

	void write(std::string_view data);

protected:
	detail::socket_type m_socket = detail::invalid_socket;
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//---------------------------------------------------------------------------------------------------------------------
tcp_server::tcp_server(int port)
	: m_port(port)
{
#ifdef _WIN32
	WSADATA wsaData;
	WSAStartup(0x101, &wsaData);
#endif

	sockaddr_in local;
	memset(&local, 0, sizeof(sockaddr_in));
	local.sin_family = AF_INET;
	local.sin_addr.s_addr = INADDR_ANY;
	local.sin_port = htons(static_cast<unsigned short>(port));

	m_socket = socket(AF_INET, SOCK_STREAM, 0);

	if (bind(m_socket, reinterpret_cast<sockaddr *>(&local), sizeof(local)) != 0)
		throw std::runtime_error("Could not bind socket!");

	if (listen(m_socket, 8))
		throw std::runtime_error("Could not listen on socket!");

}

//---------------------------------------------------------------------------------------------------------------------
tcp_server::~tcp_server()
{
	detail::close_socket(m_socket);

#ifdef _WIN32
	WSACleanup();
#endif
}

//---------------------------------------------------------------------------------------------------------------------
std::shared_ptr<tcp_client> tcp_server::accept_client()
{
	sockaddr_in from;
	socklen_t fromLen = sizeof(from);
	auto clientSocket = accept(m_socket, reinterpret_cast<struct sockaddr *>(&from), &fromLen);

	if (clientSocket == detail::invalid_socket)
		return nullptr;

	return std::make_shared<tcp_client>(clientSocket, from);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//---------------------------------------------------------------------------------------------------------------------
tcp_client::tcp_client()
{
#ifdef _WIN32
	WSADATA wsaData;
	WSAStartup(0x101, &wsaData);
#endif
}

//---------------------------------------------------------------------------------------------------------------------
tcp_client::tcp_client(detail::socket_type s, const sockaddr_in &from)
	: m_socket(s)
{

}

//---------------------------------------------------------------------------------------------------------------------
tcp_client::~tcp_client()
{
	detail::close_socket(m_socket);

#ifdef _WIN32
	WSACleanup();
#endif
}

//---------------------------------------------------------------------------------------------------------------------
bool tcp_client::connect(std::string_view address, int port)
{
	struct addrinfo *result = nullptr, *ptr = nullptr, hints;

	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	if (getaddrinfo(std::string(address).c_str(), std::to_string(port).c_str(), &hints, &result))
		return false;

	for (ptr = result; ptr != nullptr; ptr = ptr->ai_next)
	{
		m_socket = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);

		if (m_socket == detail::invalid_socket)
			return false;

		if (::connect(m_socket, ptr->ai_addr, static_cast<int>(ptr->ai_addrlen)) == detail::socket_error)
		{
			detail::close_socket(m_socket);
			m_socket = detail::invalid_socket;
			continue;
		}

		break;
	}

	freeaddrinfo(result);

	return m_socket != detail::invalid_socket;
}

//---------------------------------------------------------------------------------------------------------------------
std::string tcp_client::read()
{
	char buff[1024];
	buff[1023] = 0;

	int result = recv(m_socket, buff, 1023, 0);

	if (!result || result == detail::socket_error)
	{
		detail::close_socket(m_socket);
		throw std::runtime_error("Reading from socket failed!");
	}

	return std::string(buff);
}

//---------------------------------------------------------------------------------------------------------------------
void tcp_client::write(std::string_view data)
{
	std::string dataStr(data.data(), data.length());

	int result = send(m_socket, dataStr.data(), static_cast<int>(dataStr.size() + 1), 0);

	if (result != static_cast<int>(dataStr.size() + 1) || result == detail::socket_error)
	{
		detail::close_socket(m_socket);
		throw std::runtime_error("Writing to socket failed!");
	}
}

}
