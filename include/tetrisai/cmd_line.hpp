#pragma once

#include "base.hpp"

namespace tetrisai {

struct cmd_line
{
	std::vector<std::string> values;
	std::map<std::string, std::string, detail::no_case_comparator> options;

	cmd_line(int argc, char *argv[]);

	void print() const;

	bool has_option(std::string_view name) const;

	int option_int(std::string_view name, int defaultValue = 0) const;
	std::string option_string(std::string_view name, std::string_view defaultValue = "") const;

	std::string to_string() const
	{
		std::stringstream ss;

		for (auto &v : values)
			ss << v << " ";

		for (auto &kvp : options)
		{
			if (kvp.second.empty())
				ss << "-" << kvp.first << " ";
			else
				ss << "-" << kvp.first << "=" << kvp.second << " ";
		}

		return ss.str();
	}
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//---------------------------------------------------------------------------------------------------------------------
cmd_line::cmd_line(int argc, char *argv[])
{
	for (int i = 1; i < argc; ++i)
	{
		const char *arg = argv[i];

		if (arg[0] == '-' && arg[1] == '-')
			++arg;

		if (arg[0] == '-' || arg[0] == '/')
		{
			std::string name = arg + 1;
			std::string value;

			auto eqPos = name.find('=');
			if (eqPos == std::string::npos)
				eqPos = name.find(':');

			if (eqPos != std::string::npos)
			{
				value = name.substr(eqPos + 1);
				name = name.substr(0, eqPos);
			}

			options.insert({ name, value });
		}
		else
		{
			values.push_back(arg);
		}
	}
}

//---------------------------------------------------------------------------------------------------------------------
void cmd_line::print() const
{
	std::cerr << "Values:" << std::endl;

	for (auto &v : values)
		std::cerr << v << std::endl;

	std::cerr << std::endl << "Options:" << std::endl;

	for (auto &kvp : options)
		std::cerr << kvp.first << "=" << kvp.second << std::endl;
}

//---------------------------------------------------------------------------------------------------------------------
bool cmd_line::has_option(std::string_view name) const
{
	return options.find(std::string(name)) != options.end();
}

//---------------------------------------------------------------------------------------------------------------------
int cmd_line::option_int(std::string_view name, int defaultValue /* = 0 */) const
{
	auto iter = options.find(std::string(name));
	if (iter == options.end())
		return defaultValue;

	return std::stoi(iter->second);
}

//---------------------------------------------------------------------------------------------------------------------
std::string cmd_line::option_string(std::string_view name, std::string_view defaultValue /* = "" */) const
{
	auto iter = options.find(std::string(name));
	if (iter == options.end())
		return std::string(defaultValue);

	return iter->second;
}

}
