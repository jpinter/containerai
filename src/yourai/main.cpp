#include <tetrisai.hpp>

using namespace tetrisai;

//---------------------------------------------------------------------------------------------------------------------
int main(int argc, char *argv[])
{
	cmd_line cmd(argc, argv);

	game_server server(cmd, 1);
	game_client client(cmd, "Your AI");

	while (client.wait_for_tick())
	{
		client.send_actions("");
	}

	return 0;
}
